function emparejarArreglo( claves, valores ){
	var c = 0, r = 0;
	var obj = [];
	var registros = [ valores ];
	if( typeof valores == 'object' || typeof valores == 'array' ){
		registros = valores;
	}
	for( registro in registros ){
		obj[r] = {};
		for( valor in registros[ registro ] ){
			obj[r][ ( claves[c] !== null ) ? claves[c] : c ] = registros[ registro ][ valor ];
			c++;
		}
		r++;
		c = 0;
	}
	return obj;
}