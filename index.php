<?php
	$url   = 'http://'.$_SERVER['HTTP_HOST'];
	$raiz  = $_SERVER['DOCUMENT_ROOT'];
	$ancho = 4;

	$raiz_url = $url;
	$raiz_www = $raiz;
	$url_base = '';

	if( isset( $_REQUEST['u'] ) ){
		$raiz_url = $url.'/'.$_REQUEST['u'];
		$raiz_www = $raiz.'/'.$_REQUEST['u'];
		$url_base = $_REQUEST['u'].'/';
	}
?>
<!DOCTYPE html>
<html ng-app="aplicacion" lang='es'>
<head>
	<meta charset="utf-8">
	<title>Explorador www</title>

	<link rel="stylesheet" href="./libs/bower_components/lumx/dist/lumx.css">
	<link rel="stylesheet" href="./libs/bower_components/mdi/css/materialdesignicons.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

</head>

<body ng-init="url = '<?=$raiz_url?>'" ng-controller = "controlador">

	<ng-include src="'./inc/encabezado.html'"></ng-include>

	<div flex-container="row">

		<div flex-item="6" class="p+" style="min-height:480px;max-height:480px;" lx-scrollbar>

			<h3 class="mb"><?= $raiz_url ?></h3>

			<hr style="border:0;border-bottom:1px solid #ddd;">

			<?php

				$directorios = scandir( $raiz_www );
				$html = '<div class="mt"><div>';
				$c = 0; 
				foreach( $directorios as $dir){

					if( is_dir( $raiz_www.'/'.$dir ) && substr($dir,0,1) != '.' ){ // ocultos y ./  ../

						$c++;
						if( ($c-1) % $ancho == 0 ){
							$html .= '</div><div flex-container="row">';
						}
						if( $c == 1 && $url_base != '' ){
							/** 
								BOTON IR A HOME
							**/
							$boton = '<button class="btn btn--icon btn--fab btn--xl ';
							$boton .= 'btn--orange" lx-ripple_ onclick="irURL(\'..\')"><i class="mdi mdi-home';
							$boton .= '"></i></button>';
							$html .= '<div flex-item="'.(12/$ancho).'"><div class="p" style="overflow:h-idden;text-overflow: ellipsis;white-space: nowrap;line-height:3.5em;">'.$boton.' /</div></div>';
							$c++;
						}
						$boton = '<div class="fab"><button class="btn btn--fab btn--xl lx-ripple_ ';

						if( file_exists( $raiz_www.'/'.$dir.'/index.php') || file_exists( $raiz_www.'/'.$dir.'/index.html') ){
							/** 
							ES UNA POSIBLE APLICACION WEB (tiene almenos un index.php/html)
							**/
							$boton .= 'btn--pink" ';
							$boton .= ' lx-tooltip="'.$dir.'" tooltip-position="bottom" ';
							$boton .= ' onclick="irURL(\''.$raiz_url.'/'.$dir.'\', \'iframe_preview\')"><i class="mdi mdi-exit-to-app';
							$boton_carpeta = '<button class="btn btn--l btn--grey btn--fab" lx-ripple_ lx-tooltip="Explorar carpeta" tooltip-position="bottom" onclick="explorarCarpeta(\''.$dir.'\')"><i class="mdi mdi-folder"></i></button>';
						} else {
							/** 
							ES POSIBLEMENTE SOLO UN DIRECTORIO
							**/
							$posible_app = ( file_exists( $raiz_www.'/'.$dir.'/config.xml') && file_exists( $raiz_www.'/'.$dir.'/www') && file_exists( $raiz_www.'/'.$dir.'/platforms') );
							$boton .=  'btn--white" ';
							$boton .= ' lx-tooltip="'.$dir.'" tooltip-position="bottom" ';
							$boton .= ' onclick="irURL(\''.$dir.'\')">';
							$boton .=  '<i class="mdi mdi-folder';
							$boton_carpeta = ( $posible_app )
								? '<button class="btn btn--l btn--light-green btn--fab" lx-ripple_ lx-tooltip="Ver como móvil" tooltip-position="bottom" onclick="abrirURL(\''.$raiz_url.'/'.$dir.'/www\',\'movil\')"><i class="mdi mdi-cellphone-android"></i></button>'
								: '';
						}
						$boton .= '"></i></button>';
						/** 
							BOTONES SECUNDARIOS
						**/
						$boton .= '<div class="fab__primary fab__actions fab__actions--right">
						            <button class="btn btn--l btn--indigo btn--fab" lx-ripple_ lx-tooltip="Lanzar ventana" tooltip-position="bottom" onclick="abrirURL(\''.$raiz_url.'/'.$dir.'\')"><i class="mdi mdi-launch"></i></button>
						            '.$boton_carpeta.'
				        		</div>
				        	</div>';
						$texto_dir = ( strlen( $dir ) <= 15 ) ?  $dir :  substr($dir, 0 , 10) . '...';
						$html .= '<div flex-item="'.(12/$ancho).'"><div class="p" style="overflow:hi-dden;text-overflow: ellipsis;white-space: nowrap;line-height:3.5em;">'.$boton.' '.$texto_dir.'</div></div>';
					}

				}
				if( $c == 0){
				$boton_atras = '<button class="btn btn--icon btn--fab btn--xl ';
					$boton_atras .= 'btn--orange" lx-ripple_ onclick="irURL(\'..\')"><i class="mdi mdi-home';
					$boton_atras .= '"></i></button>';
					$boton_atras = '<div flex-item="'.(12/$ancho).'"><div class="p" style="overflow: hidden;text-overflow: ellipsis;white-space: nowrap;line-height:3.5em;">'.$boton_atras.' ..</div></div>';
					$html .= $boton_atras;
				}

				$html .= '</div></div>';
			?>
			<?= $html ?>

		</div>

		<div flex-item="6">
			<iframe id="iframe_preview"
				src="{{url}}" 
				width="98%" 
				height="99%" 
				class="m"
				style="background-color: white;min-height:480px;max-height:480px;"
			>
			</iframe>
		</div>
	</div>

	<script type="text/javascript">
		function irURL( url, destino ){
			if( typeof destino != 'undefined' ){
				$( '#'+destino ).load( function(){
					console.log( 'cargado Preview '+url);
				});
				$( '#'+destino ).attr('src', url);
			} else {
				var ir_a = ( url != '..') ? '?u=<?=$url_base?>'+url : '';
				window.location.href = '<?=$_SERVER['PHP_SELF']?>'+ir_a;
			}
		}
		function abrirURL( url, tipo ){
			var caracteristicas = 'width=1024px,height=600,top=150,left=100';
			if( tipo == 'movil' ){
				caracteristicas = 'width=400px,height=640,top=150,left=100';
			}
			caracteristicas += ',toolbars=no, scrollbars=yes' ;
			window.open ( url , 'v_app', caracteristicas );
		}
		function explorarCarpeta( url, destino ){
			var ir_a = ( url != '..') ? '?u=<?=$url_base?>'+url : '';
			window.location.href = '<?=$_SERVER['PHP_SELF']?>'+ir_a;
		}
	</script>

	<!-- Before body closing tag -->
	<script src="./libs/bower_components/jquery/dist/jquery.js"></script>
	<script src="./libs/bower_components/velocity/velocity.js"></script>
	<script src="./libs/bower_components/moment/min/moment-with-locales.js"></script>
	<script src="./libs/bower_components/angular/angular.js"></script>
	<script src="./libs/bower_components/lumx/dist/lumx.js"></script>

	<script type="text/javascript">
		var aplicacion = angular.module( 'aplicacion', ['lumx'] );
		aplicacion.controller( 'controlador', function( $scope ){

		});
	</script>

</body>
</html>